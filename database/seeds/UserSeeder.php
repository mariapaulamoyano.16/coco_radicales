<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $diego_Admin = User::create([
            'nombre'=>'diego',
            'apellido'=>'Medina',
            'dni'=>'234325355322',
            'fecha_nacimiento'=>date('Y-m-d',strtotime('27-03-1990')),
            'email'=>'xecnon@gmail.com',
            'password'=>Hash::make('1234'),// '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'estado'=>1
        ]);
        //asigno rol
        $diego_Admin->assignRole('Administrador');
        //creo user
        $hernan_Admin = User::create([
            'nombre'=>'Hernan',
            'apellido'=>'Albarracin',
            'dni'=>'12345678',
            'fecha_nacimiento'=>date('Y-m-d',strtotime('01-03-1989')),
            'email'=>'hernan.albarracin@gmail.com',
            'password'=>Hash::make('1234#2020hernan'),
            'estado'=>1
        ]);
        $hernan_Admin->assignRole('Administrador');

        $test1_miembro =User::create([
            'nombre'=>'Test1',
            'apellido'=>'ApellidoTest',
            'dni'=>'12345678',
            'fecha_nacimiento'=>date('Y-m-d',strtotime('01-03-1989')),
            'email'=>'test1@gmail.com',
            'password'=>Hash::make('1234#2020test1'),
            'estado'=>1
        ]);
        $test1_miembro->assignRole('Miembro');

        $test2_dirigente =  User::create([
            'nombre'=>'Test2',
            'apellido'=>'ApellidoTest',
            'dni'=>'12345678',
            'fecha_nacimiento'=>date('Y-m-d',strtotime('01-03-1989')),
            'email'=>'test2@gmail.com',
            'password'=>Hash::make('1234#2020test2'),
            'estado'=>1
        ]);
        $test2_dirigente->assignRole('Dirigente');
    }
}
