<?php

use Illuminate\Database\Seeder;
use App\Localidad;

class LocalidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Localidad::create([
            'localidad_nombre'=>'Municipio Burruyacu',
            'departamento_iddepartamento' =>1
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna 7 de abril',
            'departamento_iddepartamento' =>1
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Benjamín Aráoz y El Tajamar',
            'departamento_iddepartamento' =>1
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna El Chañar',
            'departamento_iddepartamento' =>1
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna El Naranjo y El Sunchal',
            'departamento_iddepartamento' =>1
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna El Puestito',
            'departamento_iddepartamento' =>1
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna El Timbó',
            'departamento_iddepartamento' =>1
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Gobernador Garmendia',
            'departamento_iddepartamento' =>1
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna La Ramada y La Cruz',
            'departamento_iddepartamento' =>1
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Piedrabuena',
            'departamento_iddepartamento' =>1
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Villa Padre Monti',
            'departamento_iddepartamento' =>1
        ]);
        Localidad::create([
            'localidad_nombre'=>'San Miguel de Tucumán ',
            'departamento_iddepartamento' =>2
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Alpachiri y El Molino ',
            'departamento_iddepartamento' =>3
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Alto Verde y Los Guchea',
            'departamento_iddepartamento' =>3
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Arcadia',
            'departamento_iddepartamento' =>3
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Concepción',
            'departamento_iddepartamento' =>3
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Gastona y Belicha',
            'departamento_iddepartamento' =>3
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna La Trinidad ',
            'departamento_iddepartamento' =>3
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Medinas',
            'departamento_iddepartamento' =>3
        ]);

        Localidad::create([
            'localidad_nombre'=>'Municipio Alderetes',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Banda del Río Salí',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Colombres',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Delfín Gallo',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna El Bracho y El Cevilar',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna El Naranjito',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna La FLorida y Luisiana',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Las Cejas',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Los Bulacio y Los Villagra',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Los Pereyra',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Los Pérez',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Los Ralos',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Ranchillos y San Miguel',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna San Andrés',
            'departamento_iddepartamento' =>4
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Famaillá',
            'departamento_iddepartamento' =>5
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Graneros',
            'departamento_iddepartamento' =>6
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Lamadrid',
            'departamento_iddepartamento' =>6
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Taco Ralo',
            'departamento_iddepartamento' =>6
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Juan Bautista Alberdi ',
            'departamento_iddepartamento' =>7
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Escaba',
            'departamento_iddepartamento' =>7
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Villa Belgrano',
            'departamento_iddepartamento' =>7
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio La Cocha',
            'departamento_iddepartamento' =>8
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna El Sacrificio',
            'departamento_iddepartamento' =>8
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Huasa Pampa',
            'departamento_iddepartamento' =>8
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Rumi Punco ',
            'departamento_iddepartamento' =>8
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna San Ignacio',
            'departamento_iddepartamento' =>8
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna San José de la Cocha',
            'departamento_iddepartamento' =>8
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Yanima',
            'departamento_iddepartamento' =>8
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Bella Vista',
            'departamento_iddepartamento' =>9
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Agua Dulce y La Soledad',
            'departamento_iddepartamento' =>9
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna El Mojón',
            'departamento_iddepartamento' =>9
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Esquina y Mancopa',
            'departamento_iddepartamento' =>9
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Estación Aráoz y Tacanas',
            'departamento_iddepartamento' =>9
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Las Talas',
            'departamento_iddepartamento' =>9
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Los Gómez',
            'departamento_iddepartamento' =>9
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Los Puestos ',
            'departamento_iddepartamento' =>9
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Manuel García Fernández',
            'departamento_iddepartamento' =>9
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Quilmes y Los Sueldos',
            'departamento_iddepartamento' =>9
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Río Colorado',
            'departamento_iddepartamento' =>9
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Sta. Rosa de Leales y Lag. Blanca',
            'departamento_iddepartamento' =>9
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Villa de Leales',
            'departamento_iddepartamento' =>9
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio San Isidro de Lules',
            'departamento_iddepartamento' =>10
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna El Manantial',
            'departamento_iddepartamento' =>10
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna San Felipe y Santa Bárbara',
            'departamento_iddepartamento' =>10
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna San Pablo y Villa Nougues',
            'departamento_iddepartamento' =>10
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Monteros',
            'departamento_iddepartamento' =>11
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Acheral',
            'departamento_iddepartamento' =>11
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Capitán Cáceres',
            'departamento_iddepartamento' =>11
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna El Cercado ',
            'departamento_iddepartamento' =>11
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna León Rougues y Santa Rosa',
            'departamento_iddepartamento' =>11
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Los Sosa',
            'departamento_iddepartamento' =>11
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Río Seco',
            'departamento_iddepartamento' =>11
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Santa Lucía',
            'departamento_iddepartamento' =>11
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Sargento Moya',
            'departamento_iddepartamento' =>11
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Soldado Maldonado',
            'departamento_iddepartamento' =>11
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Subteniente Berdina',
            'departamento_iddepartamento' =>11
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Villa Quinteros',
            'departamento_iddepartamento' =>11
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Aguilares',
            'departamento_iddepartamento' =>12
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna El Polear',
            'departamento_iddepartamento' =>12
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Los Sarmiento y Las Tipas',
            'departamento_iddepartamento' =>12
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Monte Bello',
            'departamento_iddepartamento' =>12
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Santa Ana',
            'departamento_iddepartamento' =>12
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Simoca',
            'departamento_iddepartamento' =>13
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Atahona',
            'departamento_iddepartamento' =>13
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Buena Vista',
            'departamento_iddepartamento' =>13
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Ciudacita',
            'departamento_iddepartamento' =>13
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Manuela Pedraza',
            'departamento_iddepartamento' =>13
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Monteagudo',
            'departamento_iddepartamento' =>13
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Pampa Mayo',
            'departamento_iddepartamento' =>13
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Río Chico y Nueva Trinidad',
            'departamento_iddepartamento' =>13
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna San Pedro y San Antonio',
            'departamento_iddepartamento' =>13
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Santa Cruz y La Tuna',
            'departamento_iddepartamento' =>13
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Villa Chicligasta',
            'departamento_iddepartamento' =>13
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Yerba Buena',
            'departamento_iddepartamento' =>13
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Tafí del Valle',
            'departamento_iddepartamento' =>14
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Amaicha del Valle',
            'departamento_iddepartamento' =>14
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Colalao del Valle',
            'departamento_iddepartamento' =>14
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna El Mollar',
            'departamento_iddepartamento' =>14
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Las Talitas',
            'departamento_iddepartamento' =>15
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Tafí Viejo',
            'departamento_iddepartamento' =>15
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Anca Juli',
            'departamento_iddepartamento' =>15
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna El Cadillal',
            'departamento_iddepartamento' =>15
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna La Esperanza',
            'departamento_iddepartamento' =>15
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Raco',
            'departamento_iddepartamento' =>15
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Los Nogales',
            'departamento_iddepartamento' =>15
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Trancas',
            'departamento_iddepartamento' =>16
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Choromoro',
            'departamento_iddepartamento' =>16
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna San Pedro de Colalao',
            'departamento_iddepartamento' =>16
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Tapia',
            'departamento_iddepartamento' =>16
        ]);
        Localidad::create([
            'localidad_nombre'=>'Municipio Yerba Buena – Concejal',
            'departamento_iddepartamento' =>17
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna Cevil Redondo',
            'departamento_iddepartamento' =>17
        ]);
        Localidad::create([
            'localidad_nombre'=>'Comuna San Javier',
            'departamento_iddepartamento' =>17
        ]);
    }
}
