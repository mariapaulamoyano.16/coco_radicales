<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SeccionSeeder::class);
        $this->call(DepartamentoSeeder::class);
        $this->call(LocalidadSeeder::class); 
        $this->call(CircuitoSeeder::class);
        $this->call(PermisosSeeder::class);
        $this->call(UserSeeder::class); 
        $this->call(TipoInteraccionSeeder::class); 
    }
}
