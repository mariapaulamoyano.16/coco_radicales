<?php

use App\Seccion;
use Illuminate\Database\Seeder;

class SeccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Seccion::create([
            'seccion'=>'I(CAPITAL)'
        ]);
        Seccion::create([
            'seccion'=>'II(ESTE)'
        ]);
        Seccion::create([
            'seccion'=>'III(OESTE)'
        ]);
    }
}
