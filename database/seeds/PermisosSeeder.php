<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permisos_admin = [];
        $permisos_miembro = [];

        $mod_voluntaios =  Permission::create(['name' => 'mod-voluntaios']);
        $mod_dirigentes =  Permission::create(['name' => 'mod-dirigentes']);
        $mod_asig_circuitos =  Permission::create(['name' => 'mod-asignar-circuitos']);

        array_push($permisos_admin, $mod_voluntaios);
        array_push($permisos_admin, $mod_dirigentes);
        array_push($permisos_admin, $mod_asig_circuitos);
        array_push($permisos_admin, Permission::create(['name' => 'mod-miembros']));
        array_push($permisos_admin, Permission::create(['name' => 'mod-usuarios']));
        array_push($permisos_admin, Permission::create(['name' => 'mod-roles-permisos']));
        $roleAdmin = Role::create(['name' => 'Administrador']);
        $roleAdmin->syncPermissions($permisos_admin);

        array_push($permisos_miembro, $mod_voluntaios);
        array_push($permisos_miembro, $mod_dirigentes);
        array_push($permisos_miembro, $mod_asig_circuitos);
        $roleMiembro = Role::create(['name' => 'Miembro']);
        $roleMiembro->syncPermissions($permisos_miembro);

        $roleDirigente = Role::create(['name' => 'Dirigente']);
        $roleDirigente->givePermissionTo($mod_voluntaios);

        /* PERMISOS PRA LOCALIDADES */

        Permission::create([
            'name'=>'Municipio Burruyacu',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna 7 de abril',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Benjamín Aráoz y El Tajamar',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna El Chañar',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna El Naranjo y El Sunchal',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna El Puestito',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna El Timbó',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Gobernador Garmendia',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna La Ramada y La Cruz',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Piedrabuena',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Villa Padre Monti',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'San Miguel de Tucumán ',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Alpachiri y El Molino ',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Alto Verde y Los Guchea',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Arcadia',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Concepción',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Gastona y Belicha',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna La Trinidad ',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Medinas',
            'guard_name' => 'p_localidad'
        ]);

        Permission::create([
            'name'=>'Municipio Alderetes',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Banda del Río Salí',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Colombres',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Delfín Gallo',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna El Bracho y El Cevilar',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna El Naranjito',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna La FLorida y Luisiana',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Las Cejas',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Los Bulacio y Los Villagra',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Los Pereyra',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Los Pérez',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Los Ralos',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Ranchillos y San Miguel',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna San Andrés',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Famaillá',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Graneros',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Lamadrid',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Taco Ralo',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Juan Bautista Alberdi ',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Escaba',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Villa Belgrano',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio La Cocha',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna El Sacrificio',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Huasa Pampa',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Rumi Punco ',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna San Ignacio',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna San José de la Cocha',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Yanima',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Bella Vista',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Agua Dulce y La Soledad',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna El Mojón',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Esquina y Mancopa',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Estación Aráoz y Tacanas',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Las Talas',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Los Gómez',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Los Puestos ',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Manuel García Fernández',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Quilmes y Los Sueldos',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Río Colorado',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Sta. Rosa de Leales y Lag. Blanca',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Villa de Leales',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio San Isidro de Lules',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna El Manantial',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna San Felipe y Santa Bárbara',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna San Pablo y Villa Nougues',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Monteros',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Acheral',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Capitán Cáceres',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna El Cercado ',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna León Rougues y Santa Rosa',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Los Sosa',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Río Seco',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Santa Lucía',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Sargento Moya',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Soldado Maldonado',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Subteniente Berdina',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Villa Quinteros',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Aguilares',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna El Polear',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Los Sarmiento y Las Tipas',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Monte Bello',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Santa Ana',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Simoca',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Atahona',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Buena Vista',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Ciudacita',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Manuela Pedraza',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Monteagudo',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Pampa Mayo',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Río Chico y Nueva Trinidad',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna San Pedro y San Antonio',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Santa Cruz y La Tuna',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Villa Chicligasta',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Yerba Buena',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Tafí del Valle',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Amaicha del Valle',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Colalao del Valle',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna El Mollar',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Las Talitas',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Tafí Viejo',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Anca Juli',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna El Cadillal',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna La Esperanza',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Raco',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Los Nogales',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Trancas',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Choromoro',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna San Pedro de Colalao',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Tapia',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Municipio Yerba Buena – Concejal',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna Cevil Redondo',
            'guard_name' => 'p_localidad'
        ]);
        Permission::create([
            'name'=>'Comuna San Javier',
            'guard_name' => 'p_localidad'
        ]);

/*************** PERMISOS DE CIRCUITOS **********************************/

        Permission::create([
            'name'=>'1',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'1A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'2',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'2A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'3',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'4',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'5',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'6',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'7',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'7A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'8',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'8A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'9',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'9A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'10',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'10A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'11',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'11A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'12',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'12A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'13',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'13A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'14',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'14A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'14B',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'14C',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'14D',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'15',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'15A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'15B',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'16',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'16A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'17',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'17A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'18',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'18A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'18B',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'18C',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'18D',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'18E',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'18F',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'18G',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'19',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'19A',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'20',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'21',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
        Permission::create([
            'name'=>'22',
            'guard_name'=>'p_curcitos_san_miguel'
        ]);
    }
}
