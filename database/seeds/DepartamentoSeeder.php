<?php

use Illuminate\Database\Seeder;
use App\Departamento;

class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Departamento::create([
            'departamento_nombre'=>'Burruyacú',
            'seccion_idseccion'=> 2
        ]);
        Departamento::create([
            'departamento_nombre'=>'Capital',
            'seccion_idseccion'=> 1
        ]);
        Departamento::create([
            'departamento_nombre'=>'Chicligasta',
            'seccion_idseccion'=> 3
        ]);
        Departamento::create([
            'departamento_nombre'=>'Cruz Alta',
            'seccion_idseccion'=> 2
        ]);
        Departamento::create([
            'departamento_nombre'=>'Famaillá',
            'seccion_idseccion'=> 3
        ]);
        Departamento::create([
            'departamento_nombre'=>'Graneros',
            'seccion_idseccion'=> 2
        ]);
        Departamento::create([
            'departamento_nombre'=>'Juan Bautista Alberdi',
            'seccion_idseccion'=> 3
        ]);
        Departamento::create([
            'departamento_nombre'=>'La Cocha',
            'seccion_idseccion'=> 3
        ]);
        Departamento::create([
            'departamento_nombre'=>'Leales',
            'seccion_idseccion'=> 2
        ]);
        Departamento::create([
            'departamento_nombre'=>'Lules',
            'seccion_idseccion'=> 3
        ]);
        Departamento::create([
            'departamento_nombre'=>'Monteros',
            'seccion_idseccion'=> 3
        ]);
        Departamento::create([
            'departamento_nombre'=>'Río Chico',
            'seccion_idseccion'=> 3
        ]);
        Departamento::create([
            'departamento_nombre'=>'Simoca',
            'seccion_idseccion'=> 2
        ]);
        Departamento::create([
            'departamento_nombre'=>'Tafí del Valle',
            'seccion_idseccion'=> 3
        ]);
        Departamento::create([
            'departamento_nombre'=>'Tafí Viejo',
            'seccion_idseccion'=> 3
        ]);
        Departamento::create([
            'departamento_nombre'=>'Trancas',
            'seccion_idseccion'=> 2
        ]);
        Departamento::create([
            'departamento_nombre'=>'Yerba Buena',
            'seccion_idseccion'=> 3
        ]);
    }
}
