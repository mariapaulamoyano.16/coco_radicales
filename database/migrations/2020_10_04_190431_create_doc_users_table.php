<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('documentos_iddocumento')->unsigned();
            $table->bigInteger('users_idusuario')->unsigned();
            $table->timestamps();

            $table->foreign('documentos_iddocumento')
                  ->references('iddocumento')
                  ->on('documentos')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('users_idusuario')
                  ->references('id')
                  ->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_users');
    }
}
