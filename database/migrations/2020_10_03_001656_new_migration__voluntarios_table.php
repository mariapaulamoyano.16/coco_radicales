<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NewMigrationVoluntariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voluntarios', function (Blueprint $table) {
            $table->id('idvoluntario');
            $table->string('nombre');
            $table->string('apellido');
            $table->bigInteger('dni');
            $table->bigInteger('celular')->nullable();
            $table->bigInteger('telefono')->nullable();
            $table->date('fecha_nacimiento');
            $table->string('email');
            $table->unsignedBigInteger('direccion_iddireccion');
            $table->unsignedInteger('estado');//entero sin signo hasta 10
            $table->timestamps();

        });
        Schema::table('voluntarios', function($table)
        {
            $table->foreign('direccion_iddireccion')
                        ->references('iddireccion')
                        ->on('direcciones')
                        ->onUpdate('cascade')
                        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voluntarios');
    }
}
