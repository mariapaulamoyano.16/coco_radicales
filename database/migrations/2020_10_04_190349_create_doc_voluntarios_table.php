<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocVoluntariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_voluntarios', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('documentos_iddocumento')->unsigned();
            $table->bigInteger('voluntarios_idvoluntario')->unsigned();
            $table->timestamps();

            $table->foreign('documentos_iddocumento')
                  ->references('iddocumento')
                  ->on('documentos')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('voluntarios_idvoluntario')
                  ->references('idvoluntario')
                  ->on('voluntarios')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_voluntatios');
    }
}
