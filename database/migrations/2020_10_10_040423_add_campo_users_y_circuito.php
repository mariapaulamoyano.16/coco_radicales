<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampoUsersYCircuito extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            /* $table->unsignedBigInteger('circuito_idcircuito')->nullable()->after('foto');
            $table->foreign('circuito_idcircuito')
                        ->references('idcircuito')
                        ->on('circuitos')
                        ->onUpdate('cascade')
                        ->onDelete('cascade'); */
                        
            $table->unsignedBigInteger('direccion_iddireccion')->nullable()->after('foto');
            $table->foreign('direccion_iddireccion')
                        ->references('iddireccion')
                        ->on('direcciones')
                        ->onUpdate('cascade')
                        ->onDelete('cascade');
        });
        Schema::table('circuitos', function (Blueprint $table) {
            $table->string('pdf')->nullable()->after('localidad_idlocalidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
