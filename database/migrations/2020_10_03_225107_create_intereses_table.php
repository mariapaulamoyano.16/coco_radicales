<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInteresesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intereses', function (Blueprint $table) {
            $table->id('idintereses');
            $table->string('intereses_nombre');
            $table->unsignedBigInteger('voluntario_idvoluntario');
            $table->timestamps();

            $table->foreign('voluntario_idvoluntario')
                        ->references('idvoluntario')
                        ->on('voluntarios')
                        ->onUpdate('cascade')
                        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intereses');
    }
}
