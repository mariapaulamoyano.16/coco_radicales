<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCircuitosUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('circuitos_users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('circuitos_idcircuito')->unsigned();
            $table->bigInteger('users_id')->unsigned();
            $table->timestamps();

            $table->foreign('circuitos_idcircuito')
                  ->references('idcircuito')
                  ->on('circuitos')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('users_id')
                  ->references('id')
                  ->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('circuitos_users');
    }
}
