<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class DirigentesController extends Controller
{
    public function index()
    {
        if (auth()->user()->hasRole('Administrador')) {
            $dirigentes = User::All()->where('estado',1);
           $array_dirigentes = [];
            foreach ($dirigentes as $u) {
                if ($u->hasRole('Dirigente')) {
                    array_push($array_dirigentes,$u);
                }
            }
        } else {
            if (auth()->user()->direccion_iddireccion != null) {
                $iddpto = auth()->user()->direccion->localidad->departamento->iddepartamento;
            } else {
                $iddpto = 0;
            }
            $array_dirigentes = DB::table('users')
            ->join('direcciones','direccion_iddireccion','iddireccion')
            ->join('localidades','localidad_idlocalidad','idlocalidad')
            ->join('departamentos','departamento_iddepartamento','iddepartamento')
            ->join('model_has_roles','model_id','users.id')
            ->join('roles','role_id','roles.id')
            ->where('users.estado',1)
            ->where('roles.id',3)
            ->where('iddepartamento',$iddpto)
            ->get();
            
        }
        
        return view('dirigentes.index',compact('array_dirigentes'));
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(User $User)
    {
        //
    }
    public function edit(User $User)
    {
        //
    }

    public function update(Request $request, User $User)
    {
        //
    }

    public function destroy(User $User)
    {
        //
    }
}
