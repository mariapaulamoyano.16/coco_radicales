<?php

namespace App\Http\Controllers;

use App\Direccion;
use App\Localidad;
use App\Voluntario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $u = auth()->user();
        switch ( true ) {
            case $u->hasPermissionTo('mod-voluntaios'):
                return redirect()->route('voluntario.index');
                break;
            case $u->hasPermissionTo('mod-dirigentes'):
                return redirect()->route('dirigentes.index');
                break;
            case $u->hasPermissionTo('mod-miembros'):
                return redirect()->route('miembros.index');
                break;
            case $u->hasPermissionTo('mod-usuarios'):
                return redirect()->route('usuarios.index');
                break;
            case $u->hasPermissionTo('mod-roles-permisos'):
                return redirect()->route('roles-permisos.index');
                break;
            default:
            return view('errores.permisos');
                break;
        }
/*         if (auth()->user()->hasRole('Administrador')) {
            $voluntarios = Voluntario::all()->where('estado',1);
           // dd($voluntarios);
        } else {
            //$voluntarios = Voluntario::all()->where('estado',1);
            if (auth()->user()->direccion_iddireccion != null) {
                $iddpto = auth()->user()->direccion->localidad->departamento->iddepartamento;
            } else {
                $iddpto = 0;
            }

            $voluntarios = DB::table('voluntarios')
                        ->join('direcciones','direccion_iddireccion','iddireccion')
                        ->join('localidades','localidad_idlocalidad','idlocalidad')
                        ->join('departamentos','departamento_iddepartamento','iddepartamento')
                        ->select('voluntarios.*')
                        ->where('voluntarios.estado',1)
                        ->where('iddepartamento',$iddpto)
                        ->get();
                       // dd($voluntarios);
        }       
        return view('voluntarios.index',compact('voluntarios','u')); */
    }
}
