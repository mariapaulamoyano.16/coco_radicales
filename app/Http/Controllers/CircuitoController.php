<?php

namespace App\Http\Controllers;

use App\Circuito;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CircuitoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasRole('Administrador')) {
            $circuitos = Circuito::All();
            /* $circuitos =  DB::table('circuitos')
                                ->orderBy('idcircuito', 'asc')
                                ->get(); */
        }else{
            $permissionNames = auth()->user()->getAllPermissions();

            $circuitos = DB::table('circuitos')
                        ->join('localidades','circuitos.localidad_idlocalidad','localidades.idlocalidad')
                        ->select('circuitos.*')
                        ->where(function($query) use ($permissionNames)
                        {
                            foreach ($permissionNames as $nombre) {
                                switch ($nombre->guard_name) {
                                    case 'p_localidad':
                                        $query->orWhere('localidades.localidad_nombre','=', $nombre->name);
                                        break;
                                    case 'p_curcitos_san_miguel':
                                        $query->orWhere('circuitos.circuito_nombre','=', $nombre->name);
                                        break;
                                    default:
                                        $query->Where('circuitos.circuito_nombre','=', '');
                                        break;
                                }
                            }
                        })
                        ->get();
        }       
        return view('circuitos.index',compact('circuitos'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'padronFile' => 'required|mimes:pdf',
        ],[
            'padronFile' => 'Debe seleccionar un archivo de tipo pdf',
        ]);

        if ($request->file('padronFile') != null) {
            if ( !Storage::disk('public')->exists("archivos_doc/documentos/padrones")) {
                if(Storage::disk('public')->makeDirectory("/archivos_doc/documentos/padrones" )){
                    //dd("creo la ruta");
                    $ruta = public_path()."/archivos_doc/documentos/padrones";
                }else{                
                    //dd("No creo nada devolver algo un error ");
                    return redirect()->route('circuitos.index')->with('warning','El directorio no se pudo crear');
                }            
            }else{
                //dd("entro al else ya existe la carpeta");
                $ruta = public_path()."/archivos_doc/documentos/padrones";
            }
            $file = $request->file('padronFile');
    
            $fileName = uniqid() . $file->getClientOriginalName();
        
            $file->move($ruta, $fileName);

            $circuito = Circuito::find($request->idcircuito);
            $circuito->pdf = "/archivos_doc/documentos/padrones/".$fileName;
            $circuito->save();
            return redirect()->route('circuitos.index')->with('success','Padron Cargado Correctamente');
        }else{
            return redirect()->route('circuitos.index')->with('warning','El archivo no existe');
        }       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Circuito  $circuito
     * @return \Illuminate\Http\Response
     */
    public function show(Circuito $circuito)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Circuito  $circuito
     * @return \Illuminate\Http\Response
     */
    public function edit(Circuito $circuito)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Circuito  $circuito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Circuito $circuito)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Circuito  $circuito
     * @return \Illuminate\Http\Response
     */
    public function destroy(Circuito $circuito)
    {
        //
    }
}
