<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoInteraccion extends Model
{
    protected $table = 'tipo_interaccion';

    protected $fillable = [
        'tipo_nombre','estado',
    ];
}
