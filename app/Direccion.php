<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    protected $table = 'direcciones';
    protected $primaryKey = 'iddireccion';

    protected $fillable = [
        'calle','numero','piso','dpto','manzana','casa','localidad_idlocalidad',
    ];

    //1:1 inversa
    public function voluntario()
    {
        return $this->belongsTo('App\Voluntario','voluntario_idvoluntario','idvoluntario');
    }
    //1:1 inversa
    public function user()
    {
        return $this->belongsTo('App\User','direccion_iddireccion');
    }
    //1:N invrsa
    public function localidad()
    {
        return $this->belongsTo('App\Localidad','localidad_idlocalidad','idlocalidad');
    }
}
