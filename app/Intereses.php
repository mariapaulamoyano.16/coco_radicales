<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intereses extends Model
{
    protected $table = 'intereses';
    protected $primaryKey = 'idintereses';

    protected $fillable = [
        'intereses_nombre',
    ];
    //1:N invrsa
    public function voluntario()
    {
        return $this->belongsTo('App\Voluntario','voluntario_idvoluntario','idvoluntario');
    }
}
