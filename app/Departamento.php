<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table = 'departamentos';
    protected $primaryKey = 'iddepartamento';

    protected $fillable = [
        'departamento_nombre',
    ];

    //1:N 
    public function localidades()
    {
        return $this->hasMany('App\Localidad','departamento_iddepartamento','iddepartamento');
    }
    //1:N invrsa
    public function seccion()
    {
        return $this->belongsTo('App\Seccion','seccion_idseccion','idseccion');
    }
}
