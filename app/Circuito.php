<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Circuito extends Model
{
    protected $table = 'circuitos';
    protected $primaryKey = 'idcircuito';

    protected $fillable = [
        'circuito_nombre',
    ];
    //1:1 inversa
    public function voluntario()
    {
        return $this->belongsTo('App\Voluntario','circuito_idcircuito','idcircuito');
    }
    //1:N invrsa
    public function localidad()
    {
        return $this->belongsTo('App\Localidad','localidad_idlocalidad','idlocalidad');
    }
    //N:M
    public function users()
    {
        return $this->belongsToMany('App\User', 'circuitos_users', 'circuitos_idcircuito', 'users_id');
    }
}
