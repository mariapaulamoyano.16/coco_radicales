<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    protected $table = 'documentos';
    protected $primaryKey = 'iddocumento';

    protected $fillable = [
        'doc_nombre','url',
    ];
    //N:M
    public function users()
    {
        return $this->belongsToMany('App\User', 'doc_users', 'documentos_iddocumento', 'users_idusuario');
    }
    //N:M
    public function voluntarios()
    {
        return $this->belongsToMany('App\Voluntario', 'doc_voluntarios', 'documentos_iddocumento', 'voluntarios_idvoluntario');
    }
}
