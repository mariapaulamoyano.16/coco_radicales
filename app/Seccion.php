<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seccion extends Model
{
    protected $table = 'secciones';
    protected $primaryKey = 'idseccion';

    protected $fillable = [
        'seccion',
    ];
    //1:N 
    public function departamentos()
    {
        return $this->hasMany('App\Departamento','seccion_idseccion','idseccion');
    }
}
