<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localidad extends Model
{
    protected $table = 'localidades';
    protected $primaryKey = 'idlocalidad';

    protected $fillable = [
        'localidad_nombre',
    ];

    //1:N 
    public function direcciones()
    {
        return $this->hasMany('App\Direccion','localidad_idlocalidad','idlocalidad');
    }
    //1:N 
    public function circuitos()
    {
        return $this->hasMany('App\Circuito','localidad_idlocalidad','idlocalidad');
    }
    //1:N invrsa
    public function departamento()
    {
        return $this->belongsTo('App\Departamento','departamento_iddepartamento','iddepartamento');
    }
}
