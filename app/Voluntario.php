<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voluntario extends Model
{
    protected $table = 'voluntarios';
    protected $primaryKey = 'idvoluntario';

    protected $fillable = [
        'nombre','apellido','dni','celular','telefono','fecha_nacimiento','email', 'password','estado',
    ];
    
    //1:1
    public function direccion()
    {
        return $this->hasOne('App\Direccion','iddireccion','direccion_iddireccion');
    }
    //1:1
    public function circuito()
    {
        return $this->hasOne('App\Circuito','idcircuito','circuito_idcircuito');
    }
    //1:N 
    public function intereses()
    {
        return $this->hasMany('App\Intereses','voluntario_idvoluntario','idvoluntario');
    }
    //N:M
    public function documentos()
    {
        return $this->belongsToMany('App\Documento', 'doc_voluntarios', 'voluntarios_idvoluntario', 'documentos_iddocumento');
    }
    //N:M
    public function interacciones()
    {
        return $this->belongsToMany('App\User', 'interacciones', 'voluntarios_idvoluntario', 'users_id')
                    ->withPivot('id','descripcion','fecha','estado','tipo_interaccion_id');
    }
}
