<div class="card {{-- bg-light --}} text-dark col-12">
<div class="card-header">{{$cardHeader}}</div>
    <div class="card-body" style="display: flex;
    justify-content: space-between;
    align-items: center;
    flex-wrap: wrap;">
            <div class="profile-photo-small" style="max-width: 28%;">
                <img src="" alt="perfil" id="foto"class="img-raised rounded img-fluid">
            </div>
        <div class="">
            <div class="form-group">
                <h5 class="">Direccion: </h5>
                <label id="direccion"></label>
                <h5 class="">Telefono:  </h5>
                <label id="telefono"></label>
                <h5 class="">Celular:  </h5>
                <label id="celular"></label>
                <h5 class="">Origen:  </h5>
                <label id="origen"></label>
            </div>
        </div>
        <div class="">
            <div class="form-group">
                <h5 class="">Correo:  </h5>
                <label id="correo"></label>
                <h5 class="">Localidad:  </h5>
                <label id="localidad"></label>
                <h5 class="">Departamento:  </h5>
                <label id="departamento"></label>
            </div>
        </div>
    </div>
</div>