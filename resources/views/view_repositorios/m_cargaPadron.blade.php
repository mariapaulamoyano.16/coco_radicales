<div class="modal fade" id="ModalPadron" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><strong> Cargar Padron</strong></h4>
            {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="material-icons">clear</i>
            </button> --}}
          </div>
          <div class="modal-body">

            <form action="{{ route('padron.store') }}" method="post" id="fromCargarPadron" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="idcircuito" id="idcircuito" value="">
              
              <input class="form-control" type="file" name="padronFile" id="padronFile">
            </form>

          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-link"{{--  data-dismiss="modal" --}} id="PadronCerrar">Cerrar</button>
          <button type="button" class="btn btn-link" id="PadronAceptar">Guardar</button>
          </div>
      </div>
    </div>
</div>