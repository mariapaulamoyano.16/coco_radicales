<div class="modal fade" id="ModalInteraccion" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><strong> Cargar Interacción</strong></h4>
            {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="material-icons">clear</i>
            </button> --}}
          </div>
          <div class="modal-body">

            <form action="{{ route('interaccion.store') }}" method="post" id="fromAltaInterccion">
              @csrf
              <input type="hidden" name="voluntarios_idvoluntario" id="voluntarios_idvoluntario" value="">

              <div class="form-group">
                <label for="sel1">Tipo de Contacto:</label>
                <select class="form-control" name="interaccionTipo" id="interaccionTipo">
                  
                </select>
              </div>
              <div class="form-group">
                <label for="comment">Descripción:</label>
                <textarea class="form-control" rows="5" name="interaccionDescriptcion" id="interaccionDescriptcion"></textarea>
              </div>
            </form>

          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-link"{{--  data-dismiss="modal" --}} id="InteraccionCerrar">Cerrar</button>
          <button type="button" class="btn btn-link" id="InteraccionAceptar">Guardar</button>
          </div>
      </div>
    </div>
</div>