<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Interacciones</title>
    <!-- Material Kit CSS -->
    <link href="/static/css/material-kit.css?v=2.0.4" rel="stylesheet" />
</head>
<body>

    <div class="row" >
        {{--   col-xs-12 col-sm-12 col-md-12 col-lg-12--}}
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div style="display: flex;flex-direction: column;justify-content: center;margin-top: 2%;" id="cabecera_ticket">
                {{-- <img src="/images/Icons/favicon-32x32.png" alt="Logotipo" id="logoTick" style="width: 34%; align-self: center;"> --}}
                <p class="centrado" style="display: flex;flex-direction: column;align-items: center;" id="p_cabecera_ticket">
                   REPORTE DE INTERACCIONES - VOLUNTARIOS
                </p>            
            </div>
            <div class="table-responsive" style="width: 94%;margin-left: 3%;">
                <table class="table table-striped" id="tablaInteracciones">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Fecha</th>
                            <th>Tipo</th>
                            <th>Descripcion</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($interacciones as $item)
                        <tr class="tickTR">
	                        <td class="cantidad tdTick">{{ $i++ }}</td>
	                        <td class="cantidad tdTick">{{ $item->fecha }}</td>
	                        <td class="producto tdTick">{{ $item->tipo_nombre }}</td>
	                        <td class="precio tdTick">{{ $item->descripcion }}</td>
	                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{-- <table class="Tticket table table-bordered table-hover" style="width: 100%;">
                <thead>
                    <tr class="tickTR">
                        <th class="cantidad thTick">CANT</th>
                        <th class="producto thTick">PRODUCTO</th>
                        <th class="precio thTick">$$</th>
                    </tr>
                </thead>
                <tbody id="ticket_venta">
                	@foreach ($detalle as $elem)
						<tr class="tickTR">
	                        <td class="cantidad tdTick">{{ $elem->Cantidad }}</td>
	                        <td class="producto tdTick">{{ $elem->Descripcion }}</td>
	                        <td class="precio tdTick">{{ $elem->Precio_Unitario }}</td>
	                    </tr>
                	@endforeach               
                </tbody>
            </table> --}}
        </div> 
    </div>


</body>
{{-- <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script> --}}
<script src="/static/js/core/jquery.min.js" type="text/javascript"></script>
<script src="/static/js/core/popper.min.js" type="text/javascript"></script>
<script src="/static/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
		
		window.onafterprint = function(e){
	        $(window).off('mousemove', window.onafterprint);
	        console.log('Print Dialog Closed..');
	        var redireccion="{{ URL::to('voluntarios') }}";
            window.location = redireccion;
	    };

	    window.print();
	});
</script>
</html>