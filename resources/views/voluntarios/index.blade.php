@extends('layout')
@php
    use App\Voluntario;
    use App\Circuito;

@endphp
@section('title', 'Voluntarios')

@section('seccionSaludo')
<h1>Listado de Voluntarios</h1>
{{-- <h3 class="title text-center">---</h3> --}}
@endsection

@section('contenido')
    <div class="col-md-12">

        <div class="row">
            <a href="{{route('voluntario.alta')}}"; class="btn btn-success" style="margin-left: 89%;">Nuevo Voluntario</a>
        </div>

        <div class="table-responsive" id="seccionTabla">
            <table class="table table-striped table-dark" id="tableVoluntarios">
                <thead class="thead-dark">
                <tr>
                    <th>Nombre y Apellido</th>
                    <th>Celular</th>
                    <th>Email</th>
                    <th>Circuito</th>
                    <th>Fecha de Alta</th>
                    <th>CRUD</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($voluntarios as $voluntario)  
                        <tr>
                            <td>{{ $voluntario->apellido .','.$voluntario->nombre}}</td>
                            <td>{{ $voluntario->celular}}</td>
                            <td>{{ $voluntario->email}}</td>
                            <td>{{ Circuito::find($voluntario->circuito_idcircuito)->circuito_nombre}}</td>
                            <td>{{ $voluntario->created_at}}</td>
                            <td>
                                <a href="javascript:;" class="showVoluntario" id="show_{{ $voluntario->idvoluntario}}"><i class="far fa-eye fa-2x"></i></a>
                                <a href="{{route('voluntario.edit',$voluntario->idvoluntario)}}" class="editVoluntario" id="edit_{{ $voluntario->idvoluntario}}"><i class="far fa-edit fa-2x"></i></a>
                                
                                <a href="javascript:;"
                                    onclick="eliminarVoluntario({{ $voluntario->idvoluntario}});"><i class="fas fa-times fa-2x"></i></a>
                                <form id="delete-form_{{ $voluntario->idvoluntario}}" action="{{ route('voluntario.destroy',$voluntario->idvoluntario) }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

        <div class="text-center" id="seccionDatos" style="display:none;">
            <h3 id="nombreVoluntario"></h3>
            <div class="row">
                
                @include('view_repositorios.card_info', array('cardHeader'=>'CONTACTO'))
                
                <div class="card bg-Light text-dark col-12">
                    <div class="card-header">INTERACCIONES</div>
                    <div class="card-body">
                        <div class="row" style="justify-content: flex-end;">
                            <form action="{{ route('btn_imprimir', ['id'=>1]) }}" method="get" id="fromImprimirInteracciones">
                                <input type="hidden" name="id_volutario_imprimir" id="id_volutario_imprimir">
                            </form>
                            <button class="btn btn-info" id="btn_imprimirInteracciones">Imprimir</button>
                            <button class="btn btn-primary" id="btnNewInteraccion"
                            data-toggle="modal" 
                            data-target="#ModalInteraccion">Nueva Interaccion<div class="ripple-container"></div></button>

                        </div>
                        
                        <div class="table-responsive">
                            <table class="table table-striped" id="tablaInteracciones">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Fecha</th>
                                        <th>Tipo</th>
                                        <th>Descripcion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                
            </div>
            <button class="btn btn-primary btn-sm pull-left" id="volverList">Volver al listado<div class="ripple-container"></div></button>
        </div>
        <div id="ajaxExito" class="alert alert-success message" style="display:none;bottom: 7%;text-align:center;width:80%;z-index: 1000;position: fixed;">

        </div>
        <div id="ajaxError" class="alert alert-danger message" style="display:none;bottom: 7%;text-align:center;width:80%;z-index: 1000;position: fixed;">
            
        </div>
    </div>

@endsection

@section('script')
<script>
var tableInteracciones;
$(document).ready(function() {
    setTimeout(function() {
      $('#message').fadeOut('fast');
    }, 3500);

    $('#tableVoluntarios').DataTable({
        "order": [],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        lengthMenu: [[10, 20, -1], [10, 20, 'Todos']]
        
    });
    tableInteracciones = $('#tablaInteracciones').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        lengthMenu: [[5,10, 20, -1], [5,10, 20, 'Todos']],
        
    });
} );
/* SHOW DATOS DEL VOLUNTARIO E INTERACCIONES */
$(document).on('click','.showVoluntario',function(){
    let id = $(this).prop('id').split('_')[1];
    let ruta  =  "{{ URL::to('voluntario_show/') }}"+"/"+id;
    $.ajax({
      type: 'GET',
      url: ruta,
      //data: form.serialize(),
      beforeSend: function(){ },
      error: function(jqxhr, textStatus, error){
          console.log(error);
      },
      success: function(respuesta){
        
        if (respuesta.status ) {
            tableInteracciones.clear().draw();
           // console.log(respuesta.interacciones[0].pivot);
            respuesta.interacciones.forEach((element,index) => {
                tableInteracciones.row.add( [
                    index+1,
                    element.pivot.fecha,
                    respuesta.tipoInteraccion.filter(TI => TI.id == element.pivot.tipo_interaccion_id)[0].tipo_nombre,
                    element.pivot.descripcion
                ] ).draw( false );
            });
            
            

            $('#nombreVoluntario').html(respuesta.voluntario.apellido+', '+respuesta.voluntario.nombre);
            $('#foto').attr( "src" ,respuesta.voluntario.foto);
            $('#direccion').html(respuesta.direccion.calle+' '+respuesta.direccion.numero);
            $('#telefono').html(respuesta.voluntario.telefono);
            $('#celular').html(respuesta.voluntario.celular);
            $('#origen').html(respuesta.voluntario.origen);
            $('#correo').html(respuesta.voluntario.email);
            $('#localidad').html(respuesta.localidad.localidad_nombre);
            $('#departamento').html(respuesta.departamento.departamento_nombre);
            $('#interaccionTipo').html('');
            respuesta.tipoInteraccion.forEach(elem => {
                $('#interaccionTipo').append('<option value="'+elem.id+'">'+elem.tipo_nombre+'</option>');
            });
            $('#voluntarios_idvoluntario').val(respuesta.voluntario.idvoluntario);
            $('#id_volutario_imprimir').val(respuesta.voluntario.idvoluntario);

            $('#seccionTabla').hide('slow');
            $('#seccionDatos').show('slow');
        }
      },
      dataType: 'json',
      async:true
    });

});
$(document).on('click','#InteraccionCerrar',function () {
    $('#fromAltaInterccion')[0].reset();
    $('#ModalInteraccion').modal('hide');
});
/* ALTA DE INTERACCION */
$(document).on('click','#InteraccionAceptar',function(){
    let form = $('#fromAltaInterccion');
    $.ajax({
      type: 'POST',
      url: "{{ route('interaccion.store') }}",
      data: form.serialize(),
      beforeSend: function(){ },
      error: function(jqxhr, textStatus, error){
          console.log(error);
          console.log(jqxhr);
          let id = '#ajaxError';
          $(id).html(jqxhr.responseJSON);
          mjsAlert(id);
      },
      success: function(respuesta){
        if (respuesta.status ) {
            form[0].reset();
            $('#ModalInteraccion').modal('hide');

            console.log(respuesta.interacciones);
            tableInteracciones.clear().draw();
            respuesta.interacciones.forEach((element,index) => {
                tableInteracciones.row.add( [
                    index+1,
                    element.pivot.fecha,
                    respuesta.tipoInteraccion.filter(TI => TI.id == element.pivot.tipo_interaccion_id)[0].tipo_nombre,
                    element.pivot.descripcion
                ] ).draw( false );
            });
            let id = '#ajaxExito';
            $(id).html(respuesta.mjs);
            mjsAlert(id);
        }
      },
      dataType: 'json',
      async:true
    });

});
$(document).on('click','#volverList',function(){
    $('#seccionDatos').hide('slow');
    $('#seccionTabla').show('slow');
});
$(document).on('click','#btn_imprimirInteracciones',function () {
    $('#fromImprimirInteracciones').submit();
});
function eliminarVoluntario(idvoluntario){
    if(confirm('Esta acción no podrá deshacerse. ¿Continuar?')){

        document.getElementById('delete-form_'+idvoluntario).submit();
    }
}
/* EMERGENTES */
function mjsAlert(id){
    $(id).show('slow');
    setTimeout(function() {
    $('.message').hide('slow');
    }, 3500);
}// FIN EMERGENTES //
</script>
@endsection
