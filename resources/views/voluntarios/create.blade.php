@extends('layout')

@section('title', 'Alta Voluntarios')

@section('seccionSaludo')
<h1>Nuevo Voluntario</h1>
@endsection

@section('contenido')

    <div class="col-md-12">
        <div class="row">
            <a href="{{route('voluntario.index')}}" class="btn btn-dark float-left" style="margin-bottom: 3%;">Volver</a>
        </div>
{{--         <div class="text-center">
            <h3 class="title">Nuevo Voluntario</h3>
        </div> --}}
        <form id="voluntario_store" action="{{ route('voluntario.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Nombre:</label>
                    <input type="text" class="form-control" name="nombre" 
                    value="{{old('nombre')}}">
                    </div>
                    
                    <span class="badge badge-danger">{{ $errors->first('nombre')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Apellido:</label>
                    <input type="text" class="form-control" name="apellido" 
                    value="{{old('apellido')}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('nombre')}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>DNI:</label>
                    <input type="number" class="form-control" min="1" step="1" name="dni"
                    id="dni"
                    value="{{old('dni')}}">
                    </div>
                    <span class="badge badge-danger dni_error">{{ $errors->first('dni')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Correo:</label>
                    <input type="email" class="form-control" name="email" 
                    value="{{old('email')}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('email')}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Celular:</label>
                    <input type="number" class="form-control" min="1" step="1" name="celular" 
                    value="{{old('celular')}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('celular')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Telefono:</label>
                    <input type="number" class="form-control" name="telefono">
                    </div>
                    
                </div>
            </div>
            <div class="row">

                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Departamentos:</label>
                    {{-- <input type="hidden" value="{{$departamentos}}" id="repoDptos"> --}}
                    <select class="form-control"  name="departamento" id="departamentos">
                        <option></option>
                        @foreach($departamentos as $depto)
                            <option value="{{$depto->iddepartamento}}">{{$depto->departamento_nombre}}</option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('departamento')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Localidades:</label>
                    <input type="hidden" value="{{$localidades}}" id="repoLocalidades">
                    <select class="form-control"  name="localidad" id="localidades">
                        <option></option>
                        @foreach($localidades as $localidad)
                            <option value="{{$localidad->idlocalidad}}">{{$localidad->localidad_nombre}}</option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('localidad')}}</span>
                </div>
                
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Fecha de Nacimiento:</label>
                    <input type="date" class="form-control" name="fecha_nacimiento" 
                    value="{{old('fecha_nacimiento')}}"
                    min="{{$Amin}}-01-01" max="{{$Amax}}-12-31">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('fecha_nacimiento')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Circuitos:</label>
                    <input type="hidden" value="{{$circuitos}}" id="repoCircuitos">
                    <select class="form-control"  name="circuitos" id="circuitos">
                        <option></option>
                        @foreach($circuitos as $circuito)
                            <option value="{{$circuito->idcircuito}}">{{$circuito->circuito_nombre}}</option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('circuitos')}}</span>
                </div>
            </div>

                <h5 for="sel1">Direccion:</h5>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Calle:</label>
                    <input type="text" class="form-control" name="calle" 
                    value="{{old('calle')}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('calle')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Número:</label>
                    <input type="number" class="form-control" name="numero" 
                    value="{{old('numero')}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('numero')}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Piso:</label>
                    <input type="text" class="form-control" name="piso">
                    </div>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Departamento:</label>
                    <input type="text" class="form-control" name="dpto" >
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Manzana:</label>
                    <input type="text" class="form-control" name="manzana">
                    </div>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Casa:</label>
                    <input type="text" class="form-control" name="casa">
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                <label>Origen:</label>
                <select class="form-control"  name="origen" id="origen">
                    <option></option>
                    <option value="1">IPEC</option>
                    <option value="2">Territorio</option>
                    <option value="3">Redes Sociales</option>
                    <option value="4">Sumatoria de voluntarios</option>
                    <option value="5">Pro vida</option>
                    <option value="6">Emprendedores</option>
                </select>
                </div>
                <span class="badge badge-danger">{{ $errors->first('origen')}}</span>
            </div>
            <div class="row d-flex justify-content-center" style="margin-bottom: 4%;">
                <div class="d-flex justify-content-around" >
                    <img class="img-raised rounded img-fluid" id="Foto" src="" alt="Foto" style="width: 35%;height: 35vh;display:none;">
                    <div class="">
                        <input type="file" class="form-control" id="fotoVoluntario" name="file">
                        <span class="badge badge-danger">{{ $errors->first('file')}}</span>
                    </div>
                </div>                
            </div>
{{--             <div class="row" style="margin-bottom: 4%;">
                <input type="file" class="form-control" id="fotoVoluntario" name="file">
                <span class="badge badge-danger">{{ $errors->first('file')}}</span>
            </div> --}}
            <div>
                <button type="submit" class="btn btn-info d-block" style="width:100%">Guardar</button>
            </div>
        </form>
    </div>

@endsection

@section('script')
<script>

$(document).ready(function(){
    setTimeout(function() {
      $('#message').fadeOut('fast');
    }, 3500);
    var localidades = JSON.parse( $('#repoLocalidades').val() );
    var circuitos = JSON.parse($('#repoCircuitos').val());
    $('#departamentos').change(function(e){
        var iddpto = $(this).val();
        $('#localidades').html('');
        $('#localidades').append('<option></option>');
        localidades.forEach((elem)=>{
            if(iddpto == elem.departamento_iddepartamento){
                $('#localidades').append('<option value="'+elem.idlocalidad+'">'+elem.localidad_nombre+'</option>');
            }
        });
        $('#circuitos').html('');
    });
    $('#localidades').change(function(e){
        var idlocalidad = $(this).val();
        $('#circuitos').html('');
        $('#circuitos').append('<option></option>');
        circuitos.forEach((elem)=>{
            if(idlocalidad == elem.localidad_idlocalidad){
                $('#circuitos').append('<option value="'+elem.idcircuito+'">'+elem.circuito_nombre+'</option>');
            }
        });
    });
    $("#fotoVoluntario").on("change", function(e) {
        var TmpPath = URL.createObjectURL(e.target.files[0]);
        // Mostramos la ruta temporal
        $('#Foto').attr('src', TmpPath);
        $('#Foto').show();
    });

    /* VALIDAR DNI AJAX */

    $(document).on('input','#dni',function (e) {
        let contadorDNI = $(this).val().length;

        if(contadorDNI >= 8 ){
            
            let dni = $(this).val();
            let _token = $('input[name=_token]').val();
            
            $.ajax({
                type: "POST",
                url: "{{route('ajax_dni')}}",
                data: {dni:dni,_token:_token},
                dataType: "json",
                beforeSend: function(){ 
                },
                error: function(jqxhr, textStatus, error){
                    console.log(jqxhr);
                    contadorDNI = 0;
                },
                success: function(data){
                    if (!data.status) {
                        $('.dni_error').html(data.mjs);
                    }else{
                        $('.dni_error').html('');
                        console.log(data.mjs);
                    }
                    contadorDNI = 0;
                }
            });

        }
    });
});
</script>
@endsection