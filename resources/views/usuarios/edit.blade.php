@extends('layout')

@section('title', 'Edicion usuarios')

@section('seccionSaludo')
<h1>Editar usuario</h1>
@endsection

@section('contenido')

    <div class="col-md-12">
        <div class="row">
            <a href="{{route('usuarios.index')}}" class="btn btn-dark float-left" style="margin-bottom: 3%;">Volver</a>
        </div>
{{--         <div class="text-center">
            <h3 class="title">Editar usuarios</h3>
        </div> --}}
        <form id="usuarios_update" action="{{ route('usuarios.update',$user->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Nombre:</label>
                    <input type="text" class="form-control" name="nombre" 
                    value="{{$user->nombre}}">
                    </div>
                    
                    <span class="badge badge-danger">{{ $errors->first('nombre')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Apellido:</label>
                    <input type="text" class="form-control" name="apellido" 
                    value="{{$user->apellido}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('apellido')}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>DNI:</label>
                    <input type="number" class="form-control" min="1" step="1" name="dni" 
                    value="{{$user->dni}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('dni')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Coreo:</label>
                    <input type="email" class="form-control" name="email" 
                    value="{{$user->email}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('email')}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Celular:</label>
                    <input type="number" class="form-control" min="1" step="1" name="celular" 
                    value="{{$user->celular}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('nombre')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Telefono:</label>
                    <input type="number" class="form-control" name="telefono" 
                    value="{{$user->telefono}}">
                    </div>
                    
                </div>
            </div>
            <div class="row">

                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Departamentos:</label>
                    <select class="form-control"  name="departamento" id="departamentos">
                        <option></option>
                        @foreach($departamentos as $depto)
                            <option value="{{$depto->iddepartamento}}"
                                    @if ($dpto != null && $depto->iddepartamento == $dpto->iddepartamento)
                                                    {{"selected"}}
                                    @endif>{{$depto->departamento_nombre}}</option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('departamento')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Localidades:</label>
                    <input type="hidden" value="{{$localidades}}" id="repoLocalidades">
                    <select class="form-control"  name="localidad" id="localidades">
                        <option></option>
                        @foreach($localidades as $localidad)
                            <option value="{{$localidad->idlocalidad}}"
                                    @if ($local != null && $localidad->idlocalidad == $local->idlocalidad)
                                                    {{"selected"}}
                                    @endif>{{$localidad->localidad_nombre}}</option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('localidad')}}</span>
                </div>
                
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Fecha de Nacimiento:</label>
                    <input type="date" class="form-control" name="fecha_nacimiento"
                    value="{{$user->fecha_nacimiento}}">
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('fecha_nacimiento')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Circuitos:</label>
                    <input type="hidden" value="{{$circuitos}}" id="repoCircuitos">
                    <select class="form-control"  name="circuitos" id="circuitos">
                        <option></option>
                        @foreach($circuitos as $circuito)
                            <option value="{{$circuito->idcircuito}}"
                                    @if ( count($user->circuitos) > 0 &&  $circuito->idcircuito == $user->circuitos[0]->idcircuito)
                                                    {{"selected"}}
                                    @endif>{{$circuito->circuito_nombre}}</option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('circuitos')}}</span>
                </div>
            </div>
                <h5 for="sel1">Direccion:</h5>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Calle:</label>
                    <input type="text" class="form-control" name="calle" 
                        @if ($dir != null)
                        value="{{$dir->calle}}"                        
                        @endif>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('calle')}}</span>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Número:</label>
                    <input type="number" class="form-control" name="numero" 
                        @if ($dir != null)
                        value="{{$dir->numero}}"                        
                        @endif>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('numero')}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Pisp:</label>
                    <input type="text" class="form-control" name="piso" 
                        @if ($dir != null)
                        value="{{$dir->piso}}"                        
                        @endif>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Departamento:</label>
                    <input type="text" class="form-control" name="dpto" 
                        @if ($dir != null)
                        value="{{$dir->dpto}}"                        
                        @endif>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Manzana:</label>
                    <input type="text" class="form-control" name="manzana" 
                        @if ($dir != null)
                        value="{{$dir->manzana}}"                        
                        @endif>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                    <label>Casa:</label>
                    <input type="text" class="form-control" name="casa" 
                        @if ($dir != null)
                        value="{{$dir->casa}}"                        
                        @endif>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group has-default">
                        <label>Contraseña:</label>
                        <input type="password" 
                                class="form-control" 
                                name="password">
                    </div>
                </div>
                <div class="col-lg-6 col-sm-4">
                    <div class="form-group">
                    <label>Roles:</label>
                    <select class="form-control"  name="roles" id="roles">
                        <option></option>
                        @foreach($roles as $rol)
                            <option value="{{$rol->name}}"
                                @if ($rol->name == $user->getRoleNames()[0])
                                        {{"selected"}}
                                @endif>
                                @if ($rol->id == 2)
                                Coordinador                              
                                @else                                
                                {{ $rol->name}}
                                @endif
                            </option>                        
                        @endforeach
                    </select>
                    </div>
                    <span class="badge badge-danger">{{ $errors->first('roles')}}</span>
                </div> 
            </div>
            <div class="row d-flex justify-content-center" style="margin-bottom: 4%;">
                <div class="d-flex justify-content-around" >
                    <img class="img-raised rounded img-fluid" id="Foto"src="{{$user->foto}}" alt="Foto" style="width: 35%;height: 35vh;">
                    <div class="">
                        <h5>Actualizar Foto</h5>
                        <input type="file" class="form-control" id="fotousuarios" name="file">
                        <span class="badge badge-danger">{{ $errors->first('file')}}</span>
                    </div>
                </div>              
            </div>
            <div>
                <button type="submit" class="btn btn-info d-block" style="width:100%">Actualizar</button>
            </div>
        </form>
    </div>

@endsection

@section('script')
<script>
$(document).ready(function(){
    var localidades = JSON.parse( $('#repoLocalidades').val() );
    var circuitos = JSON.parse($('#repoCircuitos').val());
    $('#departamentos').change(function(e){
        var iddpto = $(this).val();
        $('#localidades').html('');
        $('#localidades').append('<option></option>');
        localidades.forEach((elem)=>{
            if(iddpto == elem.departamento_iddepartamento){
                $('#localidades').append('<option value="'+elem.idlocalidad+'">'+elem.localidad_nombre+'</option>');
            }
        });
        $('#circuitos').html('');
    });
    $('#localidades').change(function(e){
        var idlocalidad = $(this).val();
        $('#circuitos').html('');
        $('#circuitos').append('<option></option>');
        circuitos.forEach((elem)=>{
            if(idlocalidad == elem.localidad_idlocalidad){
                $('#circuitos').append('<option value="'+elem.idcircuito+'">'+elem.circuito_nombre+'</option>');
            }
        });
    });
    $("#fotousuarios").on("change", function(e) {
        var TmpPath = URL.createObjectURL(e.target.files[0]);
        // Mostramos la ruta temporal
        $('#Foto').attr('src', TmpPath);
    });
});
</script>
@endsection