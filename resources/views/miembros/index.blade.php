@extends('layout')

@section('title', 'Miembros')

@section('seccionSaludo')
<h1>Listado de Coordinadores</h1>
{{-- <h3 class="title text-center">---</h3> --}}
@endsection

@section('contenido')
    <div class="col-md-12">
        {{-- <h3>Sin Datos</h3> --}}
        <div class="table-responsive" id="seccionTabla">
            <table class="table table-striped table-dark" id="tableMiembros">
                <thead class="thead-dark">
                <tr>
                    <th>Nombre y Apellido</th>
                    <th>Celular</th>
                    <th>Email</th>
                    <th>Circuito</th>
                    {{-- <th>CRUD</th> --}}
                </tr>
                </thead>
                <tbody>
                    @foreach($array_miembros as $miembro)  
                        <tr>
                            <td>{{ $miembro->apellido .','.$miembro->nombre}}</td>
                            <td>{{ $miembro->celular}}</td>
                            <td>{{ $miembro->email}}</td>
                            @if (count($miembro->circuitos)>0)                                
                                <td>{{ $miembro->circuitos[0]->circuito_nombre}}</td>
                            @else
                            <td>{{ '---' }}</td>                                
                            @endif
                            {{-- <td>
                                <a href="javascript:;" class="showmiembro" id="show_{{ $miembro->id}}"><i class="far fa-eye fa-2x"></i></a>
                                <a href="{{route('miembro.edit',$miembro->id)}}" class="editmiembro" id="edit_{{ $miembro->id}}"><i class="far fa-edit fa-2x"></i></a>
                                
                                <a href="javascript:;"
                                    onclick="eliminarmiembro({{ $miembro->id}});"><i class="fas fa-times fa-2x"></i></a>
                                <form id="delete-form_{{ $miembro->id}}" action="{{ route('miembro.destroy',$miembro->id) }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>                                
                            </td> --}}
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>


@endsection

@section('script')
<script>

$(document).ready(function() {
    setTimeout(function() {
      $('#message').fadeOut('fast');
    }, 3500);

    $('#tableMiembros').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        lengthMenu: [[10, 20, -1], [10, 20, 'Todos']]
        
    });
});
</script>
@endsection
