

@extends('layout')
@php
    use Spatie\Permission\Models\Role;
    use Spatie\Permission\Models\Permission;
@endphp

@section('title', 'usuarios')

@section('seccionSaludo')
<h1>Ups, Lo sentimos!!</h1>
<h3 class="title text-center">Usted no pose permisos, para esta seccion</h3>
@endsection

@section('contenido')

    <div class="col-md-12">
    </div>

@endsection

@section('script')
<script>

$(document).ready(function() {
    setTimeout(function() {
      $('#message').fadeOut('fast');
    }, 3500);
});

</script>
@endsection
