@extends('layout')

@php
    use App\User;
@endphp

@section('title', 'Principal')
@section('seccionSaludo')
<h1>Listado de Dirigentes</h1>
<h3 class="title text-center">---</h3>
@endsection

@section('contenido')
    <div class="col-md-12">
        {{-- <h3>Sin Datos</h3> --}}
        <div class="table-responsive" id="seccionTabla">
            <table class="table table-striped table-dark" id="tableDirigente">
                <thead class="thead-dark">
                <tr>
                    <th>Nombre y Apellido</th>
                    <th>Celular</th>
                    <th>Email</th>
                    <th>Circuito</th>
                    {{-- <th>CRUD</th> --}}
                </tr>
                </thead>
                <tbody>
                    @foreach($array_dirigentes as $dirigente)  
                        <tr>
                            <td>{{ $dirigente->apellido .','.$dirigente->nombre}}</td>
                            <td>{{ $dirigente->celular}}</td>
                            <td>{{ $dirigente->email}}</td>
                            @if (count(User::find($dirigente->id)->circuitos)>0)                                
                                <td>{{ User::find($dirigente->id)->circuitos[0]->circuito_nombre}}</td>
                            @else
                            <td>{{ '---' }}</td>                                
                            @endif
                            {{-- <td>
                                <a href="javascript:;" class="showdirigente" id="show_{{ $dirigente->id}}"><i class="far fa-eye fa-2x"></i></a>
                                <a href="{{route('dirigente.edit',$dirigente->id)}}" class="editdirigente" id="edit_{{ $dirigente->id}}"><i class="far fa-edit fa-2x"></i></a>
                                
                                <a href="javascript:;"
                                    onclick="eliminardirigente({{ $dirigente->id}});"><i class="fas fa-times fa-2x"></i></a>
                                <form id="delete-form_{{ $dirigente->id}}" action="{{ route('dirigente.destroy',$dirigente->id) }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>                                
                            </td> --}}
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>


@endsection

@section('script')
<script>

$(document).ready(function() {
    setTimeout(function() {
      $('#message').fadeOut('fast');
    }, 3500);

    $('#tableDirigente').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        lengthMenu: [[10, 20, -1], [10, 20, 'Todos']]
        
    });
});
</script>
@endsection
