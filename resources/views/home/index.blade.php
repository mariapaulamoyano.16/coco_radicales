@extends('layout')

@section('title', 'Principal')

@section('contenido')
    <div class="col-md-12">

        <!-- Tabs with icons on Card -->
        <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="#Voluntarios" data-toggle="tab">
                    <i class="fas fa-child fa-2x"></i>
                    Voluntarios
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#Dirigentes" data-toggle="tab">
                    <i class="fas fa-user-tie fa-2x"></i>
                    Dirigentes
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#Miembros" data-toggle="tab">
                    <i class="fas fa-user-friends fa-2x"></i>
                    Miembros
                    </a>
                </li>
                </ul>
            </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content text-center">
            <div class="tab-pane active" id="Voluntarios">
                <div class="row">
                    <a href="{{route('voluntario.alta')}}"; class="btn btn-success" style="margin-left: 89%;">Nuevo</a>
                </div>

                <div class="table-responsive" id="seccionTabla">
                    <table class="table table-striped table-dark" id="tableVoluntarios">
                        <thead class="thead-dark">
                        <tr>
                            <th>Nombre y Apellido</th>
                            <th>DNI</th>
                            <th>Email</th>
                            <th>CRUD</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($voluntarios as $voluntario)  
                                <tr>
                                    <td>{{ $voluntario->apellido .','.$voluntario->nombre}}</td>
                                    <td>{{ $voluntario->dni}}</td>
                                    <td>{{ $voluntario->email}}</td>
                                    <td>
                                        <a href="javascript:;" class="showVoluntario" id="show_{{ $voluntario->idvoluntario}}"><i class="far fa-eye fa-2x"></i></a>
                                        <a href="{{route('voluntario.edit',$voluntario->idvoluntario)}}" class="editVoluntario" id="edit_{{ $voluntario->idvoluntario}}"><i class="far fa-edit fa-2x"></i></a>
                                        
                                        <a href="javascript:;"
                                            onclick="eliminarVoluntario({{ $voluntario->idvoluntario}});"><i class="fas fa-times fa-2x"></i></a>
                                        <form id="delete-form_{{ $voluntario->idvoluntario}}" action="{{ route('voluntario.destroy',$voluntario->idvoluntario) }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>                                
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>

                <div class="text-center" id="seccionDatos" style="display:none;">
                    <h3 id="nombreVoluntario"></h3>
                    <div class="row">
                        <div class="card bg-Light text-dark col-md-4">
                            <div class="card-header">CONTACTO</div>
                            <div class="card-body row">
                                    <div class="profile-photo-small">
                                        <img src="" alt="perfil" id="foto"class="img-raised rounded img-fluid">
                                    </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h5 class="">Direccion: </h5>
                                        <label id="direccion"></label>
                                        <h5 class="">Telefono:  </h5>
                                        <label id="telefono"></label>
                                        <h5 class="">Celular:  </h5>
                                        <label id="celular"></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h5 class="">Correo:  </h5>
                                        <label id="correo"></label>
                                        <h5 class="">Localidad:  </h5>
                                        <label id="localidad"></label>
                                        <h5 class="">Departamento:  </h5>
                                        <label id="departamento"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="card bg-Light text-dark col-md-8">
                            <div class="card-header btn btn-info btn-lg" 
                                    id="btnNewInteraccion"
                                    data-toggle="modal" 
                                    data-target="#ModalInteraccion">Nueva Interaccion</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="tablaInteracciones">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Tipo</th>
                                                <th>Descripcion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>LLamada</td>
                                                <td>sadjhaskja</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Personal</td>
                                                <td>asdjbqwbkjwsa</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Llamada</td>
                                                <td>ajsdfhjsfjsk</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                        {{--  <button class="btn btn-info">Nueva Interaccion<div class="ripple-container"></div></button> --}}

                        
                    </div>
                    <button class="btn btn-primary btn-sm pull-left" id="volverList">Volver al listado<div class="ripple-container"></div></button>
                </div>
            </div>
            <div class="tab-pane" id="Dirigentes">
                <p> Sin datos aun</p>
            </div>
            <div class="tab-pane" id="Miembros">
                <p>Sin datos aun</p>
            </div>
            </div>
        </div>
        </div>
        <!-- End Tabs with icons on Card -->
    </div>


@endsection

@section('script')
<script>

$(document).ready(function() {
    setTimeout(function() {
      $('#message').fadeOut('fast');
    }, 3500);

    $('#tableVoluntarios').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        lengthMenu: [[10, 20, -1], [10, 20, 'Todos']]
        
    });
        $('#tablaInteracciones').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        // pageLength : 5,
        lengthMenu: [[5,10, 20, -1], [5,10, 20, 'Todos']]
        
    });
} );
$(document).on('click','.showVoluntario',function(){
    let id = $(this).prop('id').split('_')[1];
    let ruta  =  "{{ URL::to('voluntario_show/') }}"+"/"+id;
    $.ajax({
      type: 'GET',
      url: ruta,
      //data: form.serialize(),
      beforeSend: function(){ },
      error: function(jqxhr, textStatus, error){
          console.log(error);
          /* let a = ( jqxhr.responseJSON.errors.palabra != null )?jqxhr.responseJSON.errors.palabra:'';
            $('.palabra').html(a);

          console.log(jqxhr);
          let id = '#ajaxError';
          $(id).html('ocurio un error vuelva a intentar');
          mjsAlert(id);*/
      },
      success: function(respuesta){
        
        if (respuesta.status ) {
            $('#nombreVoluntario').html(respuesta.voluntario.apellido+', '+respuesta.voluntario.nombre);
            $('#foto').attr( "src" ,respuesta.voluntario.foto);
            $('#direccion').html(respuesta.direccion.calle+' '+respuesta.direccion.numero);
            $('#telefono').html(respuesta.voluntario.telefono);
            $('#celular').html(respuesta.voluntario.celular);
            $('#correo').html(respuesta.voluntario.email);
            $('#localidad').html(respuesta.localidad.localidad_nombre);
            $('#departamento').html(respuesta.departamento.departamento_nombre);
            $('#seccionTabla').hide('slow');
            $('#seccionDatos').show('slow');
        }
      },
      dataType: 'json',
      async:true
    });

});

$(document).on('click','#volverList',function(){
    $('#seccionDatos').hide('slow');
    $('#seccionTabla').show('slow');
});
function eliminarVoluntario(idvoluntario){
    if(confirm('Esta acción no podrá deshacerse. ¿Continuar?')){

        document.getElementById('delete-form_'+idvoluntario).submit();
    }
}
</script>
@endsection
