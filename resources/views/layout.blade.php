<!doctype html>
<html lang="es">
  <head>
    <title>@yield('title','App') - Radicales</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!--  Fonts and icons  -->
    <!-- Fonts and icons -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"> --}}
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

    <!-- Material Kit CSS -->
    <link href="{{asset('static/css/material-kit.css?v=2.0.4')}}" rel="stylesheet" />

    {{-- datatables css --}}
    <link href="{{asset('static/plugin/DataTables/datatables.min.css')}}" rel="stylesheet">

    <link href="{{ asset('plugins/TagsInput/tagsinput.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" />

  </head>
  <body>
    <nav class="navbar navbar-color-on-scroll navbar-transparent fixed-top navbar-expand-lg"  color-on-scroll="100">
        <div class="container">
            <div class="navbar-translate">
              
              @can('mod-voluntaios')                
            <a class="navbar-brand" href="{{ route('voluntario.index') }}">Voluntarios </a>
              @endcan
              @can('mod-dirigentes')                
              <a class="navbar-brand" href="{{ route('dirigentes.index') }}">Dirigentes </a>
              @endcan
              @can('mod-miembros')                
              <a class="navbar-brand" href="{{ route('miembros.index') }}">Coordinadores</a>
              @endcan
              {{-- @can('mod-circuitos')     --}}            
              <a class="navbar-brand" href="{{ route('circuitos.index') }}">Circuitos-Padron</a>
              {{-- @endcan --}}
              @can('mod-usuarios')                
              <a class="navbar-brand" href="{{ route('usuarios.index') }}">Usuarios </a>
              @endcan
              @can('mod-roles-permisos')                
              <a class="navbar-brand" href="{{ route('roles-permisos.index') }}">Roles Permisos</a>
              @endcan

              <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="navbar-toggler-icon"></span>
                  <span class="navbar-toggler-icon"></span>
                  <span class="navbar-toggler-icon"></span>
              </button> {{-- hamburguesa --}}
            </div>

            <div class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">
                  <li class="dropdown nav-item">
                    <a href="javascript:;" class="profile-photo dropdown-toggle nav-link" data-toggle="dropdown">
                      <div class="profile-photo-small">
                        @if (auth()->user()->foto != null)
                          <img src="{{auth()->user()->foto}}" alt="Circle Image" class="rounded-circle img-fluid" style="height: 6vh;">
                        @else
                          <img src="{{asset('static/img/faces/christian.jpg')}}" alt="Circle Image" class="rounded-circle img-fluid" style="height: 6vh;">
                        @endif
                      </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                      <h6 class="dropdown-header">Personal</h6>
                      <a href="javascript:;" class="dropdown-item">Perfil</a>
                      {{-- <a href="javascript:;" class="dropdown-item">Configuracion</a> --}}
                      <a href="{{route('logout')}}" class="dropdown-item"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Cerrar Sesion</a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                    </div>
                  </li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="page-header header-filter" data-parallax="true" style="background-image: url('/static/img/azul_puntos.jpg')">
      <div class="container">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto">
            <div class="brand text-center">
              @if (isset($u))
                <h1>Bienvenido</h1>
                <h3 class="title text-center">{{$u->apellido.', '.$u->nombre}}</h3>
              @else
                @yield('seccionSaludo','')                  
              @endif
            </div>
          </div>
            @if(session()->has('success'))
                <div id="message" class="alert alert-success" style="text-align:center;width:100%;">
                    {{ session('success') }}
                </div>
            @endif
            @if(session()->has('warning'))
                <div id="message" class="alert alert-warning" style="text-align:center;width:100%;">
                    {{ session('warning') }}
                </div>
            @endif
        </div>
      </div>
    </div>

    <div class="main main-raised">
      <div class="container">
        <div class="section text-center">

            @yield('contenido')

        </div>
      </div>
    </div>


    {{-- modal interaccion voluntarios--}}
    @include('view_repositorios.m_interacionVoluntarios')
    {{-- fin de modal --}}
    {{-- modal carga de padron--}}
    @include('view_repositorios.m_cargaPadron')
    {{-- fin de modal --}}
    <footer class="footer footer-default" >
      <div class="container">
        {{--     <nav class="float-left">
          <ul>
            <li>
              <a href="https://www.creative-tim.com/">
                  Creative Tim
              </a>
            </li>
          </ul>
        </nav>
        <div class="copyright float-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>, made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com/" target="blank">Creative Tim</a> for a better web.
        </div> --}}
      </div>
    </footer>
    <!--   Core JS Files   -->
    <script src="{{asset('static/js/core/jquery.min.js')}}" type="text/javascript"></script>
{{--     <script src="{{ asset('js/jquery-3.2.1.min.js')}}"></script> --}}
    <script src="{{asset('static/js/core/popper.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('static/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('static/js/plugins/moment.min.js')}}"></script>
    <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
    <script src="{{asset('static/js/plugins/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{asset('static/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
    <!--  Google Maps Plugin  -->
    {{-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> --}}
    <!-- Place this tag in your head or just before your close body tag. -->
    {{-- <script async defer src="https://buttons.github.io/buttons.js')}}"></script> --}}
    <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
    <script src="{{asset('static/js/material-kit.js?v=2.0.4')}}" type="text/javascript"></script>
        {{-- datatables script --}}
    <script src="{{asset('static/plugin/DataTables/datatables.min.js')}}" type="text/javascript"></script>

    <script src="{{ asset('plugins/TagsInput/tagsinput.js')}}"></script>
    <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
<script>
  var tiempo;
  $(document).ready(function (e) {
    tiempo = iniciarTiempo();
  });
  $(document).on('mousemove',function (e) {
    clearTimeout(tiempo)
    tiempo = iniciarTiempo()
  });
  $(document).on('scroll',function (e) {
    clearTimeout(tiempo)
    tiempo = iniciarTiempo()
  });
  function iniciarTiempo() {
    return setTimeout(() => {
      document.getElementById('logout-form').submit();
    }, 600000); //10min
  }
</script>
    @yield('script')
  </body>
</html>