@extends('layout')

@section('title', 'Edicion roles-permisoss')

@section('seccionSaludo')
<h1>Crear Rol - Permisos</h1>
@endsection

@section('contenido')

    <div class="col-md-12">
        <div class="row">
            <a href="{{route('roles-permisos.index')}}" class="btn btn-dark float-left" style="margin-bottom: 3%;">Volver</a>
        </div>

        <form id="roles_permisos_store" action="{{ route('roles-permisos.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="form-group has-default">
                        <label>Nombre del Rol:</label>
                        <input type="text" class="form-control" name="rol" placeholder="nombre-del-rol" 
                        value="{{old('rol')}}">
                        </div>
                        
                        <span class="badge badge-danger">{{ $errors->first('rol')}}</span>
                </div>
            </div>

            <div class="col-12">
                <input type="hidden" id="permisos" name="permisos">
                <div class="form-group">
                <label>Permisos:</label>
                <select class="form-control single" name="select_permisos" id="select_permisos">
                    <option></option>
                    @foreach($permisos as $permiso)
                        <option value="{{$permiso->id}}">{{$permiso->name." - ".$permiso->guard_name}}</option>  
                    @endforeach
                </select>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                <label>Permisos seleccionados:</label>
                <input class="inputTag form-control" type="text" value="" data-role="tagsinput sometext" >
            </div>

            <div>
                <button id="guardarRol" class="btn btn-info d-block" style="width:100%">Guardar</button>
            </div>
        </form>
    </div>

@endsection

@section('script')
<script>
    $('.single').select2();
    $('.select2-container').on('click',function (a) {
      $("input[type=search]").attr("placeholder", "Buscar...");
    });

$(document).on('click','#guardarRol',function(){
    event.preventDefault();
    var tags = $('.inputTag').tagsinput('items');
        $('#permisos').val(JSON.stringify(tags));

    console.log(JSON.stringify(tags));
   document.getElementById('roles_permisos_store').submit();

});
$(document).ready(function(){
    $('.inputTag').tagsinput({// CONFIGURACION
        allowDuplicates: false,
        tagClass: 'badge badge-primary',
        itemValue: 'id',
        itemText: 'text'
    });
});
$('#select_permisos').on('change',function(){ //CONTROL PARA AGREGAR TAGS
    if($(this).val()!=""){

        let obj = {
            id:$(this).val(),
            text:$('#select_permisos option:selected').html()
        };
        $('.inputTag').tagsinput('add', obj);

    } 
});

function addTags(obj){
 $('.inputTag').tagsinput('add', obj);
}

</script>
@endsection