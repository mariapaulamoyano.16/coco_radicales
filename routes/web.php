<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login.login');
})->name('login_index');
//Route::post('Home', 'LoginController@index')->name('home');

//Auth::routes();
Route::post('logeo','Auth\LoginController@login')->name('login');
Route::post('logout','Auth\LoginController@logout')->name('logout');
Route::get('/Home', 'HomeController@index')->name('home');

/* VOLUNTARIOS */
Route::group(['middleware' => ['permission:mod-voluntaios']], function () {
    Route::get('voluntarios', 'VoluntariosController@index')->name('voluntario.index');
    Route::get('voluntario_alta', 'VoluntariosController@create')->name('voluntario.alta');
    Route::post('voluntario_store', 'VoluntariosController@store')->name('voluntario.store');
    Route::post('voluntario_interaccion', 'VoluntariosController@storeInteraccion')->name('interaccion.store');
    Route::get('voluntario_show/{idvoluntario}', 'VoluntariosController@show')->name('voluntario.show');
    Route::get('voluntario_edit/{idvoluntario}', 'VoluntariosController@edit')->name('voluntario.edit');
    Route::put('voluntario_update/{idvoluntario}/up', 'VoluntariosController@update')->name('voluntario.update');
    Route::delete('voluntario_destroy/{idvoluntario}', 'VoluntariosController@destroy')->name('voluntario.destroy');
    Route::get('voluntario_interacciones_imprimir/{id}' ,'VoluntariosController@imprimirInteracciones')->name('btn_imprimir');
    Route::post('voluntario_ajax_dni' ,'VoluntariosController@ajaxValidarDni')->name('ajax_dni');
});

Route::group(['middleware' => ['permission:mod-dirigentes']], function () {
    Route::resource('dirigentes', 'DirigentesController');
});
Route::group(['middleware' => ['permission:mod-miembros']], function () {
    Route::resource('miembros', 'MiembrosController');
});
Route::group(['middleware' => ['permission:mod-roles-permisos']], function () {
    Route::resource('roles-permisos', 'RolesPermisosController');
});
Route::group(['middleware' => ['permission:mod-usuarios']], function () {
    Route::resource('usuarios', 'UsuariosController');
});

Route::get('circuitos', 'CircuitoController@index')->name('circuitos.index');
Route::post('padron_add','CircuitoController@store')->name('padron.store');


/* 
+--------+-----------+----------------------------------------+------------------------+------------------------------------------------------------------+-------------------------------+
| Domain | Method    | URI                                    | Name                   | Action                                                           | Middleware
    |
+--------+-----------+----------------------------------------+------------------------+------------------------------------------------------------------+-------------------------------+
|        | GET|HEAD  | /                                      | login_index            | Closure                                                          | web
    |
|        | GET|HEAD  | Home                                   | home                   | App\Http\Controllers\HomeController@index                        | web
    |
|        |           |                                        |                        |                                                                  | auth
    |
|        | GET|HEAD  | api/user                               |                        | Closure                                                          | api
    |
|        |           |                                        |                        |                                                                  | auth:api
    |
|        | GET|HEAD  | dirigentes                             | dirigentes.index       | App\Http\Controllers\DirigentesController@index                  | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-dirigentes 
    |
|        | POST      | dirigentes                             | dirigentes.store       | App\Http\Controllers\DirigentesController@store                  | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-dirigentes 
    |
|        | GET|HEAD  | dirigentes/create                      | dirigentes.create      | App\Http\Controllers\DirigentesController@create                 | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-dirigentes 
    |
|        | PUT|PATCH | dirigentes/{dirigente}                 | dirigentes.update      | App\Http\Controllers\DirigentesController@update                 | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-dirigentes 
    |
|        | GET|HEAD  | dirigentes/{dirigente}                 | dirigentes.show        | App\Http\Controllers\DirigentesController@show                   | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-dirigentes 
    |
|        | DELETE    | dirigentes/{dirigente}                 | dirigentes.destroy     | App\Http\Controllers\DirigentesController@destroy                | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-dirigentes 
    |
|        | GET|HEAD  | dirigentes/{dirigente}/edit            | dirigentes.edit        | App\Http\Controllers\DirigentesController@edit                   | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-dirigentes 
    |
|        | POST      | logeo                                  | login                  | App\Http\Controllers\Auth\LoginController@login                  | web
    |
|        | POST      | logout                                 | logout                 | App\Http\Controllers\Auth\LoginController@logout                 | web
    |
|        | POST      | miembros                               | miembros.store         | App\Http\Controllers\MiembrosController@store                    | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-miembros   
    |
|        | GET|HEAD  | miembros                               | miembros.index         | App\Http\Controllers\MiembrosController@index                    | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-miembros   
    |
|        | GET|HEAD  | miembros/create                        | miembros.create        | App\Http\Controllers\MiembrosController@create                   | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-miembros   
    |
|        | DELETE    | miembros/{miembro}                     | miembros.destroy       | App\Http\Controllers\MiembrosController@destroy                  | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-miembros   
    |
|        | PUT|PATCH | miembros/{miembro}                     | miembros.update        | App\Http\Controllers\MiembrosController@update                   | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-miembros   
    |
|        | GET|HEAD  | miembros/{miembro}                     | miembros.show          | App\Http\Controllers\MiembrosController@show                     | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-miembros   
    |
|        | GET|HEAD  | miembros/{miembro}/edit                | miembros.edit          | App\Http\Controllers\MiembrosController@edit                     | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-miembros   
    |
|        | GET|HEAD  | roles-permisos                         | roles-permisos.index   | App\Http\Controllers\RolesPermisosController@index               | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-roles-permisos |
|        | POST      | roles-permisos                         | roles-permisos.store   | App\Http\Controllers\RolesPermisosController@store               | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-roles-permisos |
|        | GET|HEAD  | roles-permisos/create                  | roles-permisos.create  | App\Http\Controllers\RolesPermisosController@create              | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-roles-permisos |
|        | PUT|PATCH | roles-permisos/{roles_permiso}         | roles-permisos.update  | App\Http\Controllers\RolesPermisosController@update              | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-roles-permisos |
|        | DELETE    | roles-permisos/{roles_permiso}         | roles-permisos.destroy | App\Http\Controllers\RolesPermisosController@destroy             | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-roles-permisos |
|        | GET|HEAD  | roles-permisos/{roles_permiso}         | roles-permisos.show    | App\Http\Controllers\RolesPermisosController@show                | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-roles-permisos |
|        | GET|HEAD  | roles-permisos/{roles_permiso}/edit    | roles-permisos.edit    | App\Http\Controllers\RolesPermisosController@edit                | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-roles-permisos |
|        | GET|HEAD  | usuarios                               | usuarios.index         | App\Http\Controllers\UsuariosController@index                    | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-usuarios   
    |
|        | POST      | usuarios                               | usuarios.store         | App\Http\Controllers\UsuariosController@store                    | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-usuarios   
    |
|        | GET|HEAD  | usuarios/create                        | usuarios.create        | App\Http\Controllers\UsuariosController@create                   | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-usuarios   
    |
|        | GET|HEAD  | usuarios/{usuario}                     | usuarios.show          | App\Http\Controllers\UsuariosController@show                     | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-usuarios   
    |
|        | PUT|PATCH | usuarios/{usuario}                     | usuarios.update        | App\Http\Controllers\UsuariosController@update                   | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-usuarios   
    |
|        | DELETE    | usuarios/{usuario}                     | usuarios.destroy       | App\Http\Controllers\UsuariosController@destroy                  | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-usuarios   
    |
|        | GET|HEAD  | usuarios/{usuario}/edit                | usuarios.edit          | App\Http\Controllers\UsuariosController@edit                     | web
    |
|        |           |                                        |                        |                                                                  | permission:mod-usuarios   
    |
|        | POST      | voluntario_ajax_dni                    | ajax_dni               | App\Http\Controllers\VoluntariosController@ajaxValidarDni        | web
    |
|        | GET|HEAD  | voluntario_alta                        | voluntario.alta        | App\Http\Controllers\VoluntariosController@create                | web
    |
|        | DELETE    | voluntario_destroy/{idvoluntario}      | voluntario.destroy     | App\Http\Controllers\VoluntariosController@destroy               | web
    |
|        | GET|HEAD  | voluntario_edit/{idvoluntario}         | voluntario.edit        | App\Http\Controllers\VoluntariosController@edit                  | web
    |
|        | POST      | voluntario_interaccion                 | interaccion.store      | App\Http\Controllers\VoluntariosController@storeInteraccion      | web
    |
|        | GET|HEAD  | voluntario_interacciones_imprimir/{id} | btn_imprimir           | App\Http\Controllers\VoluntariosController@imprimirInteracciones | web
    |
|        | GET|HEAD  | voluntario_show/{idvoluntario}         | voluntario.show        | App\Http\Controllers\VoluntariosController@show                  | web
    |
|        | POST      | voluntario_store                       | voluntario.store       | App\Http\Controllers\VoluntariosController@store                 | web
    |
|        | PUT       | voluntario_update/{idvoluntario}/up    | voluntario.update      | App\Http\Controllers\VoluntariosController@update                | web
    |
|        | GET|HEAD  | voluntarios                            | voluntario.index       | App\Http\Controllers\VoluntariosController@index                 | web
    |
+--------+-----------+----------------------------------------+------------------------+------------------------------------------------------------------+-------------------------------+
*/
